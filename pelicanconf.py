#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'trustable'
SITENAME = u'trustable software engineering'
SITEURL = '.'

PATH = 'content'

THEME = './theme'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

MENUITEMS = (
    ('Home', 'index.html'),
    ('Overview', 'overview.html'),
    ('Contributing', 'contributing.html'),
    ('Relevant Projects', 'projects.html'),
    ('Reading', 'reading.html'))

# Blogroll
LINKS = (('Wiki', 'https://gitlab.com/trustable/documents/wikis/home'),
         ('Mailing list', 'https://lists.trustable.io/cgi-bin/mailman/listinfo/trustable-software'),
         ('Gitlab projects', 'https://gitlab.com/trustable'),)

DEFAULT_PAGINATION = False
DISPLAY_PAGES_ON_MENU = True

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_PATHS = ['images', 'static', 'extra/CNAME']
EXTRA_PATH_METADATA = {
    'extra/CNAME': {'path': 'CNAME'},
    'images/favicon.png': {'path': 'favicon.ico'},
}

BANNER = 'images/trustable-logo-2019.png'
