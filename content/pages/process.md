Title: Process Workstream
URL: process.html
save_as: process.html

Establishing metrics to evaluate the provenance and integrity of evidence produced by software development practices and comparing the relative merits of different potential processes.
