Title: Trustable
URL: index.html
save_as: index.html

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/494/badge)](https://bestpractices.coreinfrastructure.org/projects/494)

Trustable Software is an open project with the goal of understanding risk in software development by demonstrating what we can trust software to do, when specific software can be trusted, and how to protect ourselves from the risks associated with this software.

To manage these risks at all levels of the supply chain, the Trustable project provides a collaborative environment where representatives from multiple industries can work together on measures to assess how and where software may be trusted and how to apply these when re-using software for new purposes.

With workstreams focusing on specific areas (Process, Public Constraints, Legal & Compliance and Insurance) and with contributors from each area reviewing and building upon each other’s work, Trustable applies open source development principles to establish frameworks for trust.

We will collaborate and work with existing open projects and utilise and build on their work.

If you would like to join our community, please visit the [Contributing]({filename}home.md) page to see how you can get involved.
