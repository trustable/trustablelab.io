Title: Overview
URL: overview.html
save_as: overview.html

Trustable is an open project based on community contributions that aims to make the systems and practices used to engineer software demonstrably “Trustable”, allowing the deliverables to be assessed.

Risk in software is being assessed and Insurance products, and a collaborative  insurance vehicle to insure against these risks in the commercial distribution of software deliverables are being investigated.

Trustable Software Ltd is financed through contributions from sponsors.  Potential revenue streams exist in training, audit and insurance. 

**We understand the importance of and systemic risk from faulty or improperly sourced software in our everyday lives from a legal perspective:**

- Financial loss
- Intellectual property infringement
- Personal injury and death
- Damage to property whether tangible or intangible
- Regulatory risk (e.g. GDPR)
- Damage to reputation

We propose that a deliverable can be considered “Trustable” if the systems and
practices used to engineer that deliverable meet certain criteria, which can be
organised into the following categories:

- Functionality
- Integrity
- Provenance
- Legal and compliance

The specific criteria need to be framed by our community, but our community will
start the discussion with:

- We know where it comes from
- We know how to build it
- We can reproduce it
- We know what it does
- It does what it is supposed to do
- It does not do what it is not supposed to do
- We can be confident it will not break or regress when it is updated

**The Project will create:**

- An agreed ontology or set of definitions to provide a common language for the project
- Measures and metrics – recognising there are many ways to deliver to this and that a variety of appropriate tools may adequately achieve each criterion
- Shared sets of criteria (“Public Constraints”) that deliverables should satisfy for selected domains and use cases
- A system of audit

**Our goals are to:**

- Manage the potential endemic risk of software in society through the creation of a Trustable Software ecosystem
- Create a safer world
- Create sustainable measures and associated metrics to provide evidence that systems of software engineering can be recognised as Trustable
- Create criteria that are transparent
- Enable Trustable systems to be independently assessed and audited
- Provide an auditable mark of Trustable Software for those software engineering processes
- Investigate risk in software and build an insurance product for Trustable Software
- Consider the creation of an collaborative industry insurance vehicle to support affordable insurance for Trustable Software
- Work towards support for evidence-based claims of being trustable in demanding contexts such as regulatory compliance, safety, security, quality, cost and fiduciary control
- Consider establishing an audit infrastructure for Trustable Software

**We will achieve this by:**

- Being an inclusive and global organisation
- Being a not for profit through a UK company limited by guarantee
- Being guided by an invited Advisory Board in our first year
- Building a community of engineers, lawyers and other experts actively collaborating in work streams
- Splitting our work into 4 principal work streams:
    - Process – establishing criteria with the legal and insurance work streams and building metrics to evaluate the provenance and integrity of evidence produced by software engineering practices and compare the relative merits of different processes;
    - Public Constraints – creating common and public sets of criteria that deliverables should satisfy for selected domains and use cases – this may create a number of sub work groups for those domains and use cases;
    - Legal and Compliance – establishing the legal and compliance metrics and validating measures appropriate to “trustable”; and
    - Insurance – establishing the viability of an insurable risk and the potential for an associated insurance structure and vehicle.
- Obtaining funding from sponsorship from corporate, academic and public bodies
- Moving by 2020 to an elected board
- Funding our longevity through Membership, Training, Audit and Insurance
- Protecting Trustable from being in the ownership of any one organisation

**Along the way we will:**

- Share and accept contributions
- Be open and transparent
- Listen to the many voices in and around software engineering
- Take wise counsel
- Analyse, debate and find answers to difficult questions
- Build a body of work demonstrating the state of the art across all areas of Trustable through both academic and industry white papers
- Build recognisable expertise and experts

**We acknowledge:**

- That we are committed to the principles of software freedom and the established best practices of the free software and open source communities
- That we aim to enable adoption of free and open source software in sectors with a need for Trustable systems
- The need to protect and encourage the engineering and development communities
- That software is not a sector but fundamentally underlies all sectors today
- That we must work with and welcome all sectors, particularly those where their software should be safe and secure, as contributors to Trustable
- That many voices will help us make informed choices as a diverse community
- That we will not unnecessarily re-invent and will work with a multitude of existing communities and projects and benefit from their knowledge, experience and tools
- A commitment to the principles and methodologies of the open source community
- That we will develop and refine our work through collaboration over time
- That education and expertise is critical to Trustable’s success as a project
- That we will treat each other and our communities with kindness and respect
