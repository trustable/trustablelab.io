Title: Insurance  Workstream
URL: insurance.html
save_as: insurance.html
status: hidden

Establishing insurable risk in software and the viability of an associated insurance product and collaboration-based insurance entity.
