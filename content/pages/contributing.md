Title: Contributing
URL: contributing.html
save_as: contributing.html

We try to make it as easy as possible for you to contribute to the Trustable Software project by:

- Sharing ideas, suggestions and issues via our [mailing list](https://lists.trustable.io/cgi-bin/mailman/listinfo/trustable-software)
- Suggesting improvements to the website as merge requests or issues via its [gitlab repo](https://gitlab.com/trustable/trustable.gitlab.io)
- Suggesting improvements to the wiki as merge requests or issues via its [gitlab page](https://gitlab.com/trustable/overview/wikis/home)
