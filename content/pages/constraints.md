Title: Public Constraints Workstream
URL: constraints.html
save_as: constraints.html
status: hidden

Creating common and public sets of criteria that deliverables should satisfy for selected domains and use cases.  This may create a number of subordinate work groups for those domains and use cases.
